<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Comment extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    const TYPE_FORUM = 1;
    const TYPE_DOCUMENT = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'post_id', 'type', 'message'
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function forum() {
        return $this->belongsTo('App\Forum', 'post_id');
    }

    public function document() {
        return $this->belongsTo('App\Document', 'post_id');
    }

    public function recommends() {
        return $this->hasMany('App\CommentRecommend', 'comment_id')
            ->where('type', CommentRecommend::TYPE_FORUM)
            ->where('status', CommentRecommend::STATUS_RECOMMENDED);
    }
}
