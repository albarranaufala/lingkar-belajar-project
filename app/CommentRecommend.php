<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class CommentRecommend extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    const TYPE_FORUM = 1;
    const TYPE_DOCUMENT = 2;
    const STATUS_RECOMMENDED = 1;
    const STATUS_UNRECOMMEND = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'comment_id', 'type', 'status'
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function comment() {
        return $this->belongsTo('App\Comment', 'comment_id');
    }

    public $timestamps = false;
}
