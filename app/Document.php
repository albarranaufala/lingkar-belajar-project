<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class Document extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'title', 'description', 'type_id', 'subject_id', 'attachment'
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function subject() {
        return $this->belongsTo('App\Subject', 'subject_id');
    }

    public function fileArray() {
        $names = explode('|', $this->attachment);

        return collect($names);
    }

    public function type() {
        return $this->belongsTo('App\DocumentType', 'type_id');
    }

    public function comments(){
        return $this->hasMany('App\Comment', 'post_id')->where('type', Comment::TYPE_DOCUMENT);
    }

    public function recommends() {
        return $this->hasMany('App\PostRecommend', 'post_id')
            ->where('type', PostRecommend::TYPE_DOCUMENT)
            ->where('status', PostRecommend::STATUS_RECOMMENDED);
    }
}
