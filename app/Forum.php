<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Forum extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'title', 'description', 'subject_id', 'attachment'
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function subjects() {
        $subjectIdCollection = collect(explode(',', $this->subject_id));
        $subjectsCollection = $subjectIdCollection->map(function($id){
            return Subject::findOrFail($id);
        });
        $subjects = (object) [
            'names' => implode(', ', $subjectsCollection->map(function($subject){
                return $subject->name;
            })->toArray()),
            'data' => $subjectsCollection
        ];
        return $subjects;
    }

    public function comments(){
        return $this->hasMany('App\Comment', 'post_id')->where('type', Comment::TYPE_FORUM);
    }

    public function recommends() {
        return $this->hasMany('App\PostRecommend', 'post_id')
            ->where('type', PostRecommend::TYPE_FORUM)
            ->where('status', PostRecommend::STATUS_RECOMMENDED);
    }
}
