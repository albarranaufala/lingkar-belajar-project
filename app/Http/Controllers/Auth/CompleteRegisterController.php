<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Majority;
use App\University;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompleteRegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'auth.uncomplete']);
    }

    public function completeRegister(){
        $majorities = Majority::all();
        $universities = University::all();
        return view('auth.complete', compact('majorities', 'universities'));
    }

    public function storeCompleteRegister(Request $request){
        $user = Auth::user();
        $user->majority_id = $request->majority;
        $user->university_id = $request->university;
        $user->save();
        return back();
    }
}
