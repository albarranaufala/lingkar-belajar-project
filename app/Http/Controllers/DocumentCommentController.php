<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Document;
use App\Http\Requests\DocuCommentStoreRequest;
use App\Http\Requests\DocumentCommentStoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DocumentCommentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'auth.complete', 'verified']);
    }
    
    public function store(DocumentCommentStoreRequest $request, Document $document){
        Comment::create($request->validated() + [
            'type' => Comment::TYPE_DOCUMENT,
            'user_id' => Auth::id(),
            'post_id' => $document->id,
        ]);

        return back()
            ->andNotify('success', 'Komentar berhasil dibuat!');
    }
}
