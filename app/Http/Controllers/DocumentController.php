<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DocumentStoreRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Document;
use App\Subject;
use App\Comment;
use App\DocumentType;
use App\PostRecommend;
use App\Services\FileService;
use App\User;
use Illuminate\Support\Facades\DB;

class DocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'auth.complete', 'verified']);
    }

    public function index(Request $request)
    {
        $documents = Document::all();
        $subjects = Subject::all();
        $types = DocumentType::all();
        $firstFiveStudents = User::where('university_id', Auth::user()->university->id)
            ->orderBy('point', 'DESC')
            ->take(5)
            ->get();

        return view('documents.index', compact('documents', 'subjects', 'types', 'firstFiveStudents'));
    }

    public function show(Document $document) 
    {
        return view('documents.show', compact('document'));
    }

    public function store(DocumentStoreRequest $request, FileService $fileService) 
    {
        if($request->hasFile('file')) {
            foreach($request->file('file') as $file) {
                $path[] = $fileService->setFile($file)
                    ->setFolderName('DocumentAttachments')
                    ->upload();
            }

        } else {
            $path = [];
        }
        
        Document::create($request->validated() + [
            'user_id' => Auth::id(),
            'type_id' => $request->type, // jenis dokumen
            'subject_id' => $request->subject,
            'attachment' => implode('|', $path)
        ]);

        return redirect(route('documents.index'))
            ->andNotify('success', 'Dokumen berhasil diunggah!');
    }

    public function delete(Document $document)
    {
        $document->delete();

        return view();
    }
}
