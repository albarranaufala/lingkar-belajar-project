<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Forum;
use App\Http\Requests\ForumCommentStoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ForumCommentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'auth.complete', 'verified']);
    }
    
    public function store(ForumCommentStoreRequest $request, Forum $forum){
        Comment::create($request->validated() + [
            'type' => Comment::TYPE_FORUM,
            'user_id' => Auth::id(),
            'post_id' => $forum->id,
        ]);

        return back()
            ->andNotify('success', 'Komentar berhasil dibuat!');
    }
}
