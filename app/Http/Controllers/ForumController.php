<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ForumStoreRequest;
use Illuminate\Support\Facades\Auth;

use App\Forum;
use App\Subject;
use App\Services\FileService;
use App\User;

class ForumController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'auth.complete', 'verified']);
    }

    public function index(Request $request)
    {
        $forums = Forum::all();
        $subjects = Subject::all();
        $firstFiveStudents = User::where('university_id', Auth::user()->university->id)
            ->orderBy('point', 'DESC')
            ->take(5)
            ->get();

        return view('forums.index', compact('forums', 'subjects', 'firstFiveStudents'));
    }

    public function show(Forum $forum){

        return view('forums.show', compact('forum'));
    }

    public function store(ForumStoreRequest $request, FileService $fileService) {
        if($request->hasFile('image')) {
            $path = $fileService->setFile($request->file('image'))
                ->setFolderName('ForumAttachments')
                ->upload();
        } else {
            $path = null;
        }
        
        Forum::create($request->validated() + [
            'user_id' => Auth::id(),
            'subject_id' => implode(',', $request->subjects),
            'attachment' => $path
        ]);



        return redirect(route('forums.index'))
            ->andNotify('success', 'Diskusi berhasil dibuat!')
            ->andNotify('warning', 'Poin anda berkurang 10 poin!');
    }

    public function delete(Forum $forum)
    {
        $forum->delete();

        return view();
    }
}
