<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class GoogleController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('google')->stateless()->redirect();
    }
 
    public function callback()
    {
        if (Auth::check()) {
            return redirect('/forums');
        }
 
        $oauthUser = Socialite::driver('google')->stateless()->user();
        $user = User::where('google_id', $oauthUser->id)->first();

        if ($user) {
            Auth::loginUsingId($user->id);

            return redirect('/forums');
        } else {
            $newUser = User::create([
                'name' => $oauthUser->name,
                'email' => $oauthUser->email,
                'google_id'=> $oauthUser->id,
                'point' => 100,
                'rank' => 'Mahasiswa Baru',
                'email_verified_at' => now(),
                // Password will not being used
                'password' => md5($oauthUser->token),
            ]);

            Auth::login($newUser);

            return redirect('/forums');
        }
    }
}
