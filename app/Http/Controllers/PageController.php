<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Forum;
use App\Subject;
use App\User;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

}
