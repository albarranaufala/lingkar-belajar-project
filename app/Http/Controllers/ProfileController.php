<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdateRequest;
use App\Majority;
use App\University;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'auth.complete']);
    }

    public function show(User $user){
        $userForums = $user->forums->each(function($forum){
            $forum->activityType = 'forum';
        });
        $userDocuments = $user->documents->each(function($document){
            $document->activityType = 'document';
        });
        $user->activities = $userForums->concat($userDocuments)->sortByDesc('created_at');
        
        return view('profiles.show', compact('user'));
    }
    public function edit(){
        $majorities = Majority::all();
        $universities = University::all();
        return view('profiles.edit', compact('majorities', 'universities'));
    }
    public function update(ProfileUpdateRequest $request){
        Auth::user()->update([
            'name' => $request->name,
            'email' => $request->email,
            'majority_id' => $request->majority,
            'university_id' => $request->university,
        ]);

        return redirect(route('profiles.show', Auth::id()))
            ->andNotify('success', 'Profile berhasil diedit!');
    }
}
