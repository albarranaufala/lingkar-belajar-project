<?php

namespace App\Http\Controllers;

use App\User;
use App\Forum;
use App\Document;
use App\Comment;
use App\CommentRecommend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\PostRecommend;

class RecommendController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'auth.complete', 'verified']);
    }

    public function recommendForum(Request $request, Forum $forum) {
        $user = Auth::user();
        $userRecommends = $user->recommendForum($forum);

        if($userRecommends->count()){
            $userRecommend = $userRecommends->first();

            $userRecommend->status = $userRecommend->status == PostRecommend::STATUS_RECOMMENDED 
                ? PostRecommend::STATUS_UNRECOMMEND : PostRecommend::STATUS_RECOMMENDED;
            $userRecommend->save();
        }
        else{
            $userRecommend = PostRecommend::create([
                'type' => PostRecommend::TYPE_FORUM,
                'user_id' => Auth::id(),
                'post_id' => $forum->id,
                'status' => PostRecommend::STATUS_RECOMMENDED
            ]);
        }

        return response()->json([
            'data' => [
                'userRecommend' => $userRecommend,
                'recommendCount' => $forum->recommends->count()
            ]
        ]);
    }

    public function recommendDocument(Request $request, Document $document) {
        $user = Auth::user();
        $userRecommends = $user->recommendDocument($document);
        if($userRecommends->count()){
            $userRecommend = $userRecommends->first();
            $userRecommend->status = $userRecommend->status == PostRecommend::STATUS_RECOMMENDED 
                ? PostRecommend::STATUS_UNRECOMMEND : PostRecommend::STATUS_RECOMMENDED;
            $userRecommend->save();
        }
        else{
            $userRecommend = PostRecommend::create([
                'type' => PostRecommend::TYPE_DOCUMENT,
                'user_id' => Auth::id(),
                'post_id' => $document->id,
                'status' => PostRecommend::STATUS_RECOMMENDED
            ]);
        }
        return response()->json([
            'data' => [
                'userRecommend' => $userRecommend,
                'recommendCount' => $document->recommends->count()
            ]
        ]);
    }

    public function recommendCommentForum(Request $request, Forum $forum, Comment $comment) {
        $user = Auth::user();
        $userRecommends = $user->recommendCommentForum($comment);

        if($userRecommends->count()){
            $userRecommend = $userRecommends->first();

            $userRecommend->status = $userRecommend->status == CommentRecommend::STATUS_RECOMMENDED 
                ? CommentRecommend::STATUS_UNRECOMMEND : CommentRecommend::STATUS_RECOMMENDED;
            $userRecommend->save();
        }
        else{
            $userRecommend = CommentRecommend::create([
                'type' => CommentRecommend::TYPE_FORUM,
                'user_id' => Auth::id(),
                'comment_id' => $comment->id,
                'status' => CommentRecommend::STATUS_RECOMMENDED
            ]);
        }

        return response()->json([
            'data' => [
                'userRecommend' => $userRecommend,
                'recommendCount' => $comment->recommends->count()
            ]
        ]);
    }
}
