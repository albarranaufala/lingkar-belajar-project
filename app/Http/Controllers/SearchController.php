<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Document;
use App\DocumentType;
use App\Forum;
use App\Services\SearchService;
use App\Subject;
use Symfony\Component\Console\Input\Input;

class SearchController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'auth.complete', 'verified']);
    }

    public function searchForum(Request $request, SearchService $searchService){
        $subjects = Subject::all();
        $forumResults = $searchService
            ->setSubjectsByIds($request->subjectIds)
            ->setKeyword($request->keyword)
            ->searchForum();
        
        return view('forums.result', compact('subjects', 'forumResults'));
    }

    public function searchDocument(Request $request, SearchService $searchService){
        $subjects = Subject::all();
        $types = DocumentType::all();
        $documentResults = $searchService
            ->setSubjectsByIds($request->subjectIds)
            ->setKeyword($request->keyword)
            ->setTypesByIds($request->typeIds)
            ->searchDocument();
        
        return view('documents.result', compact('subjects', 'documentResults', 'types'));
    }
}