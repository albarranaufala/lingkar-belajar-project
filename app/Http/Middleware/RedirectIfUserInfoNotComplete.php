<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;

class RedirectIfUserInfoNotComplete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if ($user->majority_id || $user->university_id) {
            return $next($request);
        }
        
        return redirect(RouteServiceProvider::REGISTER_COMPLETE);
    }
}
