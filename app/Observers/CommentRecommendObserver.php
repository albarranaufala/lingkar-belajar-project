<?php

namespace App\Observers;

use App\CommentRecommend;
use App\User;
use App\Forum;
use App\Comment;
use App\Services\RankService;

class CommentRecommendObserver
{
    /**
     * Handle the comment recommend "creating" event.
     *
     * @param  \App\CommentRecommend  $commentRecommend
     * @return void
     */
    public function created(CommentRecommend $commentRecommend)
    {
        $user = $commentRecommend->comment->user;
        if($commentRecommend->type == CommentRecommend::TYPE_FORUM){
            $user->point += 2;
        }

        $rankService = new RankService;

        $rankService->setUser($user)
            ->rankUpdate();
    }

    /**
     * Handle the comment recommend "updated" event.
     *
     * @param  \App\CommentRecommend  $commentRecommend
     * @return void
     */
    public function updated(CommentRecommend $commentRecommend)
    {
        $user = $commentRecommend->comment->user;
        if($commentRecommend->type == CommentRecommend::TYPE_FORUM){
            $user->point += $commentRecommend->status == CommentRecommend::STATUS_RECOMMENDED ? 2 : -2;
        }

        $rankService = new RankService;

        $rankService->setUser($user)
            ->rankUpdate(); 
    }

    /**
     * Handle the comment recommend "deleted" event.
     *
     * @param  \App\CommentRecommend  $commentRecommend
     * @return void
     */
    public function deleted(CommentRecommend $commentRecommend)
    {
        //
    }

    /**
     * Handle the comment recommend "restored" event.
     *
     * @param  \App\CommentRecommend  $commentRecommend
     * @return void
     */
    public function restored(CommentRecommend $commentRecommend)
    {
        //
    }

    /**
     * Handle the comment recommend "force deleted" event.
     *
     * @param  \App\CommentRecommend  $commentRecommend
     * @return void
     */
    public function forceDeleted(CommentRecommend $commentRecommend)
    {
        //
    }
}
