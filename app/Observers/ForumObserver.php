<?php

namespace App\Observers;

use Illuminate\Support\Facades\Auth;

use App\Forum;
use App\Services\RankService;

class ForumObserver
{
    /**
     * Handle the forum "created" event.
     *
     * @param  \App\Forum  $forum
     * @return void
     */
    public function creating(Forum $forum)
    {
        $user = Auth::user();

        if($user->point > 10) {
            $user->point -= 10;

            $rankService = new RankService;
            
            $rankService->setUser($user)
                ->rankUpdate();
        } else {
            return false;
        }
    }

    /**
     * Handle the forum "updated" event.
     *
     * @param  \App\Forum  $forum
     * @return void
     */
    public function updated(Forum $forum)
    {
        //
    }

    /**
     * Handle the forum "deleted" event.
     *
     * @param  \App\Forum  $forum
     * @return void
     */
    public function deleted(Forum $forum)
    {
        //
    }

    /**
     * Handle the forum "restored" event.
     *
     * @param  \App\Forum  $forum
     * @return void
     */
    public function restored(Forum $forum)
    {
        //
    }

    /**
     * Handle the forum "force deleted" event.
     *
     * @param  \App\Forum  $forum
     * @return void
     */
    public function forceDeleted(Forum $forum)
    {
        //
    }
}
