<?php

namespace App\Observers;

use App\PostRecommend;
use App\Forum;
use App\Services\RankService;
use App\User;

class PostRecommendObserver
{
    /**
     * Handle the post recommend "created" event.
     *
     * @param  \App\PostRecommend  $postRecommend
     * @return void
     */
    public function created(PostRecommend $postRecommend)
    {
        if($postRecommend->type == PostRecommend::TYPE_FORUM){
            $user = $postRecommend->forum->user;
            $user->point += 2;
        } else {
            $user = $postRecommend->document->user;
        }

        $rankService = new RankService;

        $rankService->setUser($user)
            ->rankUpdate();
    }

    /**
     * Handle the post recommend "updated" event.
     *
     * @param  \App\PostRecommend  $postRecommend
     * @return void
     */
    public function updated(PostRecommend $postRecommend)
    {
        if($postRecommend->type == PostRecommend::TYPE_FORUM){
            $user = $postRecommend->forum->user;
            $user->point += $postRecommend->status == PostRecommend::STATUS_RECOMMENDED ? 2 : -2;
        } else {
            $user = $postRecommend->document->user;
        }

        $rankService = new RankService;

        $rankService->setUser($user)
            ->rankUpdate();
    }

    /**
     * Handle the post recommend "deleted" event.
     *
     * @param  \App\PostRecommend  $postRecommend
     * @return void
     */
    public function deleted(PostRecommend $postRecommend)
    {
        //
    }

    /**
     * Handle the post recommend "restored" event.
     *
     * @param  \App\PostRecommend  $postRecommend
     * @return void
     */
    public function restored(PostRecommend $postRecommend)
    {
        //
    }

    /**
     * Handle the post recommend "force deleted" event.
     *
     * @param  \App\PostRecommend  $postRecommend
     * @return void
     */
    public function forceDeleted(PostRecommend $postRecommend)
    {
        //
    }
}
