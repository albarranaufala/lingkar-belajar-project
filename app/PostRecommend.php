<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class PostRecommend extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    const TYPE_FORUM = 1;
    const TYPE_DOCUMENT = 2;
    const STATUS_RECOMMENDED = 1;
    const STATUS_UNRECOMMEND = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'post_id', 'type', 'status' 
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function forum() {
        return $this->belongsTo('App\Forum', 'post_id');
    }

    public function document() {
        return $this->belongsTo('App\Document', 'post_id');
    }

    public $timestamps = false;
}
