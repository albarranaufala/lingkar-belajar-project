<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

use App\Comment;
use App\PostRecommend;
use App\CommentRecommend;
use App\Forum;
use App\Observers\PostRecommendObserver;
use App\Observers\CommentRecommendObserver;
use App\Observers\ForumObserver;
use Illuminate\Http\RedirectResponse;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Forum::observe(ForumObserver::class);
        PostRecommend::observe(PostRecommendObserver::class);
        CommentRecommend::observe(CommentRecommendObserver::class);
        Schema::defaultStringLength(191);

        RedirectResponse::macro('andNotify', function (string $type, string $message) {
            /** @var \Illuminate\Http\RedirectResponse $this */
            $notifications = session('notifications', []);
            $notifications[] = (object) compact('type', 'message');

            return $this->with("notifications", $notifications);
        });
    }

}
