<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class FileService {
    private $file;
    private $folderName;

    public function setFile(UploadedFile $file) : FileService
    {
        $this->file = $file;

        return $this;
    }

    public function setFolderName($folderName) : FileService
    {
        $this->folderName = $folderName;

        return $this;
    }

    public function upload()
    {
        $fileNameExt = $this->file->getClientOriginalName();
        $fileName = pathinfo($fileNameExt, PATHINFO_FILENAME);
        $ext = $this->file->getClientOriginalExtension();
        $fileNameStore = $fileName.'_'.time().'_'.Auth::id().'.'. $ext;
        $path = Storage::putFileAs('public/'.$this->folderName ,$this->file, $fileNameStore);

        return $path;
    }
}