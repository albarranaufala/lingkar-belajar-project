<?php

namespace App\Services;

use App\User;

class RankService {
    private $user;

    public function setUser(User $user) : RankService
    {
        $this->user = $user;
        
        return $this;
    }

    public function rankUpdate(){
        $userPoint = $this->user->point;
        if($userPoint < 120){
            $rank = 'Mahasiswa Baru';
        } 
        else if($userPoint < 240) {
            $rank = 'Mahasiswa Teladan';
        } 
        else {
            $rank = 'Mahasiswa Berprestasi';
        }

        $this->user->rank = $rank;
        $this->user->save();
    }
}