<?php

namespace App\Services;

use App\Forum;
use App\Document;
use App\DocumentType;
use App\Subject;

class SearchService {
    private $subjects;
    private $keyword;
    private $types;

    public function __construct()
    {
        $this->subjects = collect([]);
        $this->types = collect([]);
        $this->keyword = '';
    }

    public function setSubjects($subjects): SearchService
    {
        if(!$subjects){
            return $this;
        }

        $this->subjects = $subjects;

        return $this;
    }

    public function setSubjectsByIds($subjectIds): SearchService
    {
        if(!$subjectIds){
            return $this;
        }

        $subjectIdCollection = collect($subjectIds);
        $subjectsCollection = $subjectIdCollection->map(function($id){
            return Subject::findOrFail($id);
        });

        $this->subjects = $subjectsCollection;

        return $this;
    }

    public function setTypes($types): SearchService
    {
        if(!$types){
            return $this;
        }

        $this->types = $types;

        return $this;
    }

    public function setTypesByIds($typeIds): SearchService
    {
        if(!$typeIds){
            return $this;
        }

        $typeIdCollection = collect($typeIds);
        $typesCollection = $typeIdCollection->map(function($id){
            return DocumentType::findOrFail($id);
        });

        $this->types = $typesCollection;

        return $this;
    }

    public function setKeyword($keyword){
        $this->keyword = $keyword;

        return $this;
    }

    public function searchForum()
    {
        $forums = Forum::where('title', 'like', "%$this->keyword%")
            ->get()
            ->filter(function($forum){
                return $forum->subjects()->data
                    ->intersect($this->subjects)
                    ->count() == $this->subjects->count() ?? $forum->subjects()->data->count();
            });
        return $forums;
    }

    public function searchDocument()
    {
        $documents = Document::where('title', 'like', "%$this->keyword%")
            ->get()
            ->filter(function($document){
                if(!$this->subjects->count()){
                    return true;
                }

                return $this->subjects
                    ->contains($document->subject);
            })
            ->filter(function($document){
                if(!$this->types->count()){
                    return true;
                }

                return $this->types
                    ->contains($document->type);
            });
        return $documents;
    }
}