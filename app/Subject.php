<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Subject extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'majority_id'
    ];

    public function majority() {
        return $this->belongsTo('App\Majority', 'majority_id');
    }

    public function forums() {
        return $this->hasMany('App\Forum');
    }

    public function documents() {
        return $this->hasMany('App\Document');
    }
}
