<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'majority_id', 'university_id', 'password', 'point', 'rank', 'google_id', 'email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public $timestamps = false;

    public function forums() {
        return $this->hasMany('App\Forum');
    }

    // Model majority sudah terhudung belongsto unversity. 
    // Jika tidak pakai university_id sepertinya tetap dapat nge-get univ melalui majority
    public function university() {
        return $this->belongsTo('App\University', 'university_id');
    }
    
    public function majority() {
        return $this->belongsTo('App\Majority', 'majority_id');
    }

    public function documents() {
        return $this->hasMany('App\Document');
    }

    public function comments() {
        return $this->hasMany('App\Comment');
    }

    public function commentsCount(){
        return $this->hasManyThrough('App\Comment','App\Forum','user_id','post_id');
    }

    public function recommendForum(Forum $forum){
        return $this->hasMany('App\PostRecommend', 'user_id')
            ->where('post_id', $forum->id)
            ->where('type', PostRecommend::TYPE_FORUM);
    }

    public function recommendDocument(Document $document){
        return $this->hasMany('App\PostRecommend', 'user_id')
            ->where('post_id', $document->id)
            ->where('type', PostRecommend::TYPE_DOCUMENT);
    }

    public function recommendCommentForum(Comment $comment){
        return $this->hasMany('App\CommentRecommend', 'user_id')
            ->where('comment_id', $comment->id)
            ->where('type', CommentRecommend::TYPE_FORUM);
    }
}
