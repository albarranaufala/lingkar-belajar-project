<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AddDiscussion extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public $subjects;
    
    public function __construct($subjects)
    {
        $this->subjects = $subjects;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.add-discussion');
    }
}
