<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AddDocument extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public $subjects, $types;
    
    public function __construct($subjects, $types)
    {
        $this->subjects = $subjects;
        $this->types = $types;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.add-document');
    }
}
