<?php

namespace App\View\Components;

use Illuminate\View\Component;

class DocumentItem extends Component
{
    public $document;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($document)
    {
        $this->document = $document;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.document-item');
    }
}
