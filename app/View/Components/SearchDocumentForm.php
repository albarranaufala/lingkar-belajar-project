<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SearchDocumentForm extends Component
{
    public $subjects, $types;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($subjects, $types)
    {
        $this->subjects = $subjects;
        $this->types = $types;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.search-document-form');
    }
}
