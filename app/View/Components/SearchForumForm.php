<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SearchForumForm extends Component
{
    public $subjects;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($subjects)
    {
        $this->subjects = $subjects;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.search-forum-form');
    }
}
