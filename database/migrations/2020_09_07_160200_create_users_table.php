<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('google_id')->nullable();
            $table->integer('majority_id')->nullable();
            $table->integer('university_id')->nullable(); // Model majority sudah terhudung belongsto unversity. Jika tidak pakai university_id sepertinya tetap dapat nge-get univ melalui majority
            $table->string('password');
            $table->double('point')->nullable();
            $table->string('rank')->nullable(); // 1. Mahasiswa Kentang (cont.)
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
    