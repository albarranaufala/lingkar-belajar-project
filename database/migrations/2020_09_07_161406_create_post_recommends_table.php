<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostRecommendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_recommends', function (Blueprint $table) {
            $table->id();
            $table->integer('type'); // 1. Forum, 2. Dokumen
            $table->integer('user_id');
            $table->integer('post_id'); // ID forum atau ID document
            $table->integer('status')->nullable(); // Status on = 1, off = 0
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post_recommends', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
