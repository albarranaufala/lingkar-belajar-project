<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentRecommendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_recommends', function (Blueprint $table) {
            $table->id();
            $table->integer('type'); // 1. Forum, 2. Dokumen
            $table->integer('user_id');
            $table->integer('comment_id');
            $table->string('status')->nullable(); // Status on = 1, off = 0
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comment_recommends', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
