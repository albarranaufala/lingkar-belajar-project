<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UniversitySeeder::class);
        $this->call(MajoritySeeder::class);
        $this->call(SubjectSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ForumSeeder::class);
        $this->call(DocumentTypeSeeder::class);
    }
}
