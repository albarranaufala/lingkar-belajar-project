<?php

use Illuminate\Database\Seeder;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('document_types')->insert([
            'name' => 'Tugas'
        ]);

        DB::table('document_types')->insert([
            'name' => 'Ujian Tengah Semester'
        ]);

        DB::table('document_types')->insert([
            'name' => 'Ujian Akhir Semester'
        ]);

        DB::table('document_types')->insert([
            'name' => 'Quiz'
        ]);
    }
}
