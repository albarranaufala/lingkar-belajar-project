<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ForumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forums')->insert([
            'user_id' => 1,
            'title' => 'Metode untuk ‘Emphatize’ dalam Pemikiran Desain',
            'description' => 'Dalam materi pemikiran desain yang saya peroleh di prodi Informatika, ada istilah yang disebut dengan ‘emphatize’. Menurut kalian yang sudah berpengalaman, apa metode yang paling tepat untuk bisa berempati dengan persona user yang akan kita selesaikan masalahnya dalam pengembangan sebuah aplikasi?',
            'subject_id' => '1',
            'created_at' => Carbon::create('2020', '09', '23')
        ]);
    }
}
