<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MajoritySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('majorities')->insert([
            ['name' =>'Akuntansi','university_id' => 1],
            ['name' =>'Arsitektur','university_id' => 1],
            ['name' =>'Ekonomi Islam','university_id' => 1],
            ['name' =>'Ekonomi Pembangunan','university_id' => 1],
            ['name' =>'Farmasi','university_id' => 1],
            ['name' =>'Ilmu Hukum','university_id' => 1],
            ['name' =>'Ilmu Komunikasi','university_id' => 1],
            ['name' =>'Kedokteran','university_id' => 1],
            ['name' =>'Psikologi','university_id' => 1],
            ['name' =>'Teknik Industri','university_id' => 1],
            ['name' =>'Informatika','university_id' => 1],
            ['name' =>'Hubungan Internasional','university_id' => 1],
        ]);
    }
}
