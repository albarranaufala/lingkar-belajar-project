<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'John Doe',
            'email' => 'johndoe@john.doe',
            'majority_id' => 1,
            'university_id' => 1,
            'point' => 100,
            'rank' => 'Mahasiswa Baru',
            'password' => Hash::make('123123123'),
            'point' => 100
        ]);
    }
}
