@extends('layouts.app')

@section('content')
<div class="container pt-3">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Lengkapi Data Diri
                </div>
                <div class="card-body">
                    <x-inputs.input 
                        label="Nama" 
                        name="name" 
                        type="text"
                        :value="Auth::user()->name"
                        disabled
                    />
                    <x-inputs.input 
                        label="Email" 
                        name="email" 
                        type="email"
                        :value="Auth::user()->email"
                        disabled
                    />
                    <form action="{{route('register.complete.store')}}" method="POST">
                        @csrf
                        <x-inputs.select 
                            required 
                            label="Jurusan" 
                            name="majority" 
                            placeholder="Pilih jurusan"
                            search
                            :option-values=" $majorities->map(fn($majority) => $majority->id)"
                            :option-names=" $majorities->map(fn($majority) => $majority->name)" 
                        />
                        <x-inputs.select 
                            required 
                            label="Universitas" 
                            name="university" 
                            placeholder="Pilih universitas"
                            search
                            :option-values=" $universities->map(fn($university) => $university->id)"
                            :option-names=" $universities->map(fn($university) => $university->name)" 
                        />
                        <div class="text-right">
                            <button class="btn btn-primary" type="submit">
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection