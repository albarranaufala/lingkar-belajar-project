@extends('layouts.app')

@section('content')
<div class="container pt-3 h-100 d-flex">
    <div class="row justify-content-center align-items-center" style="flex-grow: 1">
        <div class="col-md-8 px-5 d-none d-lg-block">
            <p class="headline-subtitle">merasa belajar daring tidak efektif?</p>
            <div class="headline-title">
                Buat Belajar Daringmu Jadi Berkesan!
            </div>
            <ul class="benefit-list">
                <li><i class="fas fa-check-circle" style="color:#0091f2"></i> Bertukar dokumen materi matakuliah</li>
                <li><i class="fas fa-check-circle" style="color:#0091f2"></i> Membuka diskusi untuk hal-hal yang tidak kamu pahami</li>
                <li><i class="fas fa-check-circle" style="color:#0091f2"></i> Berikan responmu dan jadi mahasiswa terbaik di Kampus</li>
            </ul>
        </div>
        <div class="col-md-6 col-lg-4">
            <x-login-form/>
        </div>
    </div>
</div>
@endsection

@push('style')
    <style>
        .h-100 {
            min-height: 90vh;
        }
        .headline-title {
            font-size: 3rem;
            font-weight: bold;
            color: #042d4a;
        }
        .benefit-list {
            list-style:none;
            padding: 1rem;
        }
        .benefit-list li {
            margin-bottom: 0.45rem;
            font-size: 1.25rem
        }
        .headline-subtitle {
            letter-spacing: 5px;
            font-size: 1rem;
            text-transform: uppercase;
        }
    </style>
@endpush
