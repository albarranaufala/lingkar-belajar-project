@extends('layouts.app')

@section('content')
<div class="container pt-3">
    <div class="row justify-content-center h-100 align-items-center">
        <div class="col-md-8">
            <div class="card shadow card-body">
                <div class="text-center my-3"><b style="font-size: 1.5rem">Daftarkan sebuah akun baru</b></div>
                <form method="POST" action="{{route('register')}}" class="card-body" >
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <x-inputs.input 
                                label="Nama" 
                                name="name" 
                                type="text"
                                placeholder="Masukkan Nama Anda"
                                required
                            />

                            <x-inputs.select 
                                required 
                                label="Jurusan" 
                                name="majority" 
                                placeholder="Pilih jurusan"
                                search
                                :option-values=" $majorities->map(fn($majority) => $majority->id)"
                                :option-names=" $majorities->map(fn($majority) => $majority->name)" 
                            />
                            <x-inputs.select 
                                required 
                                label="Universitas" 
                                name="university" 
                                placeholder="Pilih universitas"
                                search
                                :option-values=" $universities->map(fn($university) => $university->id)"
                                :option-names=" $universities->map(fn($university) => $university->name)" 
                            />
                        </div>
                        <div class="col-md-6">
                            <x-inputs.input 
                                label="Email" 
                                name="email" 
                                type="email"
                                placeholder="Masukkan Email Anda"
                                required
                            />
                            <x-inputs.input 
                                label="Password" 
                                name="password" 
                                type="password"
                                placeholder="Masukkan Password Anda"
                                required
                            />
                            <x-inputs.input 
                                label="Konfirmasi Password" 
                                name="password_confirmation" 
                                type="password"
                                placeholder="Masukkan Lagi Password Anda"
                                required
                            />
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-6 mb-2">
                            <a href="{{route('google')}}" class="btn btn-danger btn-block">Login dengan Google</a>
                        </div>
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-primary btn-block">Daftar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
    <style>
        .h-100 {
            min-height: 90vh;
        }
    </style>
@endpush
