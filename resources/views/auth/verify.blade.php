@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mt-3">
        <div class="col-md-6">
            <div class="card mb-3">
                <div class="card-body text-center p-5">
                    <h2 class="font-weight-bold mb-3">Verifikasi Email Anda!</h2>
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif
                    <div class="mb-3">{{Auth::user()->email}}</div>
                    <div class="mb-3">
                        Kami telah mengirimkan email ke alamat email tersebut.
                        Silahkan cek inbox atau spam untuk <strong>memverifikasi email anda</strong>.
                    </div>
                    <form method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        Jika kamu belum menerima email,
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">
                            klik di sini untuk mengirim ulang email verifikasi.
                        </button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
