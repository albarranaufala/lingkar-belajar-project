<div {{ $attributes->merge(['class' => 'card']) }}>
    <div class="card-body">
        <a class="btn btn-block rounded-lg btn-light" data-toggle="collapse" data-target="#collapseAddDiscussion" type="button" aria-expanded="false" aria-controls="collapseAddDiscussion">
            <i class="fas fa-plus"></i> 
            Buat diskusi
        </a>
    </div>
    <div class="collapse" id="collapseAddDiscussion">
        <form class="card-body" method="POST" action="{{ route('forums.store') }}" enctype = "multipart/form-data">
            @csrf
            <x-inputs.input 
                label="Judul" 
                name="title" 
                placeholder="Masukkan judul diskusi" 
                required
            />
            <x-inputs.textarea 
                label="Deskripsi" 
                name="description" 
                placeholder="Masukkan deskripsi..."
            />
            <x-inputs.select 
                required 
                label="Mata Kuliah" 
                name="subjects" 
                placeholder="Pilih mata kuliah"
                search
                :option-values=" $subjects->map(fn($subject) => $subject->id)"
                :option-names=" $subjects->map(fn($subject) => $subject->name)" 
                multiple
            />

            <div id="attachment-preview" class="w-100 mb-3 p-3 position-relative">

            </div>

            <div class="d-flex justify-content-between flex-wrap">
                <div>
                    <label for="file-input" class="btn btn-outline-primary position-relative" style="cursor: pointer">
                        <i class="fa fa-image"></i>
                        Lampirkan Foto
                        <input class="w-100" type="file" name="image" id="file-input" style="opacity:0; position:absolute; z-index:-1; top:0; left:0; right:0; bottom:0;" accept="image/png, image/jpeg">
                    </label>
                </div>
                <div class="ml-auto">
                    <a class="btn btn-light mr-2" data-toggle="collapse" href="#collapseAddDiscussion" role="button" aria-expanded="false" aria-controls="collapseAddDiscussion">Batal</a>
                    <button class="btn btn-primary {{Auth::user()->point < 10 ? 'is-invalid' : ''}}" {{Auth::user()->point < 10 ? 'disabled' : ''}} type="submit">
                        <i class="fas fa-coins"></i> 10 poin
                    </button>
                    @if (Auth::user()->point < 10)
                    <small class="invalid-feedback text-right" role="alert">
                        Poin anda kurang
                    </small>
                    @endif
                </div>
            </div>
        </form>
    </div>
</div>

@push('scripts')
    <script>
        $(document).ready(function() {
            const attachmentPreview = $('#attachment-preview');
            function updatePreview(input) {
                if (input.files && input.files[0]) {
                    let reader = new FileReader();
                    
                    reader.onload = function(e) {
                        attachmentPreview.html(
                            `
                                <img src="${e.target.result}" alt="..." class="w-100 rounded">
                                <button type="button" id="btn-remove-file" class="position-absolute btn btn-danger" style="bottom:2rem; right:2rem"><i class="fa fa-trash"></i></button>
                            `);
                    }
                    
                    reader.readAsDataURL(input.files[0]); // convert to base64 string
                } else {
                    attachmentPreview.html('');
                }
            }

            $("#file-input").change(function() {
                updatePreview(this);
            });

            $("body").delegate('#btn-remove-file', "click", function(){
                const fileInput = $("#file-input");
                fileInput.val('');
                updatePreview(fileInput);
            });
        });
    </script>
@endpush