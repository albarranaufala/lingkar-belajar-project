<div {{ $attributes->merge(['class' => 'card']) }}>
    <div class="card-body">
        <a class="btn btn-block rounded-lg bg-light" data-toggle="collapse" data-target="#collapseAddDiscussion" type="button" aria-expanded="false" aria-controls="collapseAddDiscussion">
            <i class="fas fa-plus"></i> 
            Unggah Dokumen
        </a>
    </div>
    <div class="collapse" id="collapseAddDiscussion">
        <form class="card-body" method="POST" action="{{ route('documents.store') }}" enctype="multipart/form-data">
            @csrf
            <x-inputs.input 
                label="Judul" 
                name="title" 
                placeholder="Masukkan judul diskusi" 
                required
            />
            <div class="card {{$errors->has('file') ? 'is-invalid mb-0' : ''}}">
                <div class="card-body text-center">
                    <div class="file-container">
                        <label for="file" class="file-input-container btn btn-outline-primary position-relative mb-0" style="cursor: pointer">
                            <i class="fa fa-file"></i>
                            Unggah Dokumen
                            <input id="file" class="w-100 file-input" type="file" name="file[]" style="opacity:0; position:absolute; z-index:1; top:0; left:0; right:0; bottom:0; cursor: pointer;" required>
                        </label>
                    </div>
                </div>
            </div>
            @error('file')
                <small class="invalid-feedback mb-3" role="alert">
                    {{ $message }}
                </small>
            @enderror
            <x-inputs.textarea
                label="Deskripsi" 
                name="description" 
                placeholder="Masukkan deskripsi..."
            />
            <x-inputs.select 
                required 
                label="Mata Kuliah" 
                name="subject" 
                placeholder="Pilih mata kuliah"
                search
                :option-values=" $subjects->map(fn($subject) => $subject->id)"
                :option-names=" $subjects->map(fn($subject) => $subject->name)"
            />
            <x-inputs.select 
                required 
                label="Jenis Dokumen" 
                name="type" 
                placeholder="Pilih jenis dokumen"
                search
                :option-values=" $types->map(fn($type) => $type->id)"
                :option-names=" $types->map(fn($type) => $type->name)"
            />

            <div class="d-flex justify-content-end">
                <div>
                    <a class="btn btn-light mr-2" data-toggle="collapse" href="#collapseAddDiscussion" role="button" aria-expanded="false" aria-controls="collapseAddDiscussion">Batal</a>
                    <button class="btn btn-primary" type="submit">Unggah Dokumen</button>
                </div>
            </div>
        </form>
    </div>
</div>

@push('scripts')
    <script>
        $(document).ready(function() {
            function addFile(input) {
                if (input.files && input.files[0]) {
                    const fileContainer = input.parentElement.parentElement;
                    const labelInput = input.parentElement;
                    const filesContainer = fileContainer.parentElement;
                    const fileName = getFileName(input.value)
                    const fileExtension = getFileExt(input.value)
                    
                    labelInput.classList.toggle('invisible');
                    labelInput.classList.toggle('position-absolute');

                    const previewFile = previewFileHTML(fileName, fileExtension);
                    const newNode1 = document.createElement('div');
                    newNode1.innerHTML = previewFile;
                    fileContainer.appendChild(newNode1);
                    
                    const newNode2 = document.createElement('div');
                    newNode2.classList.add('file-container');
                    newNode2.innerHTML = buttonAddFile();
                    filesContainer.appendChild(newNode2);
                }
            }

            function removeFile(input){
                const fileContainer = input.parentElement.parentElement.parentElement.parentElement;
                fileContainer.innerHTML = '';
            }

            $("body").delegate('.file-input', "change", function(){
                addFile(this);
            });

            $("body").delegate('.btn-remove-file', "click", function(){
                removeFile(this);
            });

            function getFileName(filePath){
                return filePath.substr(filePath.lastIndexOf('\\') + 1).split('.')[0];
            }

            function getFileExt(filePath){
                return filePath.split('.')[1];
            }

            function previewFileHTML(fileName, fileExtension){
                return  `
                        <div class="card bg-info text-light mb-3">
                            <div class="card-body d-flex justify-content-between">
                                <span>${fileName}[${fileExtension}]</span>
                                <span class="btn-remove-file" style="cursor:pointer"><i class="fa fa-trash"></i></span>
                            </div>
                        </div>
                    `
            }

            function buttonAddFile(){
                return `
                        <label for="file" class="file-input-container btn btn-outline-primary position-relative mb-0" style="cursor: pointer">
                            <i class="fa fa-file"></i>
                            Tambah dokumen
                            <input id="file" class="w-100 file-input" type="file" name="file[]" style="opacity:0; position:absolute; z-index:1; top:0; left:0; right:0; bottom:0; cursor: pointer;">
                        </label>
                    `
            }
            
        });
    </script>
@endpush