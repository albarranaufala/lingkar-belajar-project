<div {{ $attributes->merge(['class' => 'card']) }}>
    <div class="card-body">
        <div class="row mb-2">
            <div class="col-12 d-flex justify-content-between">   
                <div>
                    @foreach($forum->subjects()->data as $subject)
                        <a class="badge badge-secondary px-2" 
                            href="{{route('forums.search', ['subjectIds' => $subject->id])}}">
                            {{$subject->name}}
                        </a>
                    @endforeach
                </div>
                <small class="text-truncate">{{$forum->created_at->diffForHumans()}}</small>
            </div>
        </div>
        <div class="row mb-3">
            <a class="col-12" href="{{route('forums.show', $forum->id)}}">
                <h4 class="font-weight-bold text-dark mb-0">
                    {{$forum->title}}
                </h4>
            </a>
        </div>
        @if ($forum->description)
        <div class="row mb-3">
            <div class="col-12">
                <p class="text-truncate mb-0">{{$forum->description}}</p>
            </div>
        </div>
        @endif
        @if ($forum->attachment)
        <div class="row mb-3">
            <div class="col-12">
                <div class="badge badge-success">
                    Lampiran tersedia
                </div>
            </div>
        </div>
        @endif
        <div class="dropdown-divider my-3"></div>
        <div class="row">
            <div class="col-12 d-flex justify-content-between align-items-end">
                <div class="d-flex align-items-center flex-fill" style="min-width: 0">
                    <img style="height: 40px; width:40px; object-fit:cover" src="{{ Avatar::create($forum->user->name)->toBase64() }}" alt="..." class="rounded-circle mr-3">
                    <div class="d-flex flex-column flex-fill" style="min-width: 0">
                        <a href="{{ route('profiles.show', $forum->user->id) }}" class="font-weight-bold mb-0 text-truncate text-dark">
                            {{ $forum->user->name }}
                        </a>
                        <small class="text-truncate">
                            {{$forum->user->majority->name}} 
                            &#8231; 
                            {{$forum->user->university->name}}
                        </small>
                    </div>
                </div>
                <div class="d-flex align-items-center">
                    <i class="{{Auth::user()->recommendForum($forum)->count() ?
                        (Auth::user()->recommendForum($forum)->first()->status == App\PostRecommend::STATUS_RECOMMENDED ?
                            'fas text-danger'
                            : 'far')
                        : 'far'
                    }} fa-heart mr-1"></i> {{ $forum->recommends->count() }}
                    <i class="far fa-comment-alt mr-1 ml-3"></i> {{ count($forum->comments) }}
                </div>
            </div>
        </div>
    </div>
</div>