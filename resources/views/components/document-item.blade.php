<div {{ $attributes->merge(['class' => 'card']) }}>
    <div class="card-body">
        <div class="row mb-2">
            <div class="col-12 d-flex justify-content-between">
                <div>
                    <a class="badge badge-secondary px-2" 
                        href="{{route('documents.search', ['subjectIds' => $document->subject->id])}}">
                        {{$document->subject->name}}
                    </a>
                    <a class="badge badge-success px-2" 
                        href="{{route('documents.search', ['typeIds' => $document->type->id])}}">
                        {{$document->type->name}}
                    </a>
                </div>
                <small class="text-truncate">{{$document->created_at->diffForHumans()}}</small>
            </div>
        </div>
        <div class="row mb-3">
            <a class="col-12" href="{{route('documents.show', $document->id)}}">
                <h4 class="font-weight-bold text-dark mb-0">
                    {{$document->title}}
                </h4>
            </a>
        </div>
        @if ($document->description)
        <div class="row mb-3">
            <div class="col-12">
                <p class="text-truncate mb-0">{{$document->description}}</p>
            </div>
        </div>
        @endif
        <div class="row mb-3">
            <div class="col-12">
                <div class="badge badge-info text-light">
                    {{$document->fileArray()->count()}} Dokumen
                </div>
            </div>
        </div>
        <div class="dropdown-divider my-3"></div>
        <div class="row">
            <div class="col-12 d-flex justify-content-between align-items-end">
                <div class="d-flex align-items-center flex-fill" style="min-width: 0">
                    <img style="height: 40px; width:40px; object-fit:cover" src="{{ Avatar::create($document->user->name)->toBase64() }}" alt="..." class="rounded-circle mr-3">
                    <div class="d-flex flex-column flex-fill" style="min-width: 0">
                        <a href="{{ route('profiles.show', $document->user->id) }}" class="font-weight-bold mb-0 text-truncate text-dark">
                            {{ $document->user->name }}
                        </a>
                        <small class="text-truncate">
                            {{$document->user->majority->name}} 
                            &#8231; 
                            {{$document->user->university->name}}
                        </small>
                    </div>
                </div>
                <div class="d-flex align-items-center">
                    <i class="{{Auth::user()->recommendDocument($document)->count() ?
                        (Auth::user()->recommendDocument($document)->first()->status == App\PostRecommend::STATUS_RECOMMENDED ?
                            'fas text-danger'
                            : 'far')
                        : 'far'
                    }} fa-heart mr-1"></i> {{ $document->recommends->count() }}
                    <i class="far fa-comment-alt mr-1 ml-3"></i> {{ count($document->comments) }}
                </div>
            </div>
        </div>
    </div>
</div>