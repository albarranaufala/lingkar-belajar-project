<form class="card shadow" action="{{route('login')}}" method="POST">
    @csrf
    <div class="card-body py-4 px-4">
        <div class="mb-3">
            <b style="font-size: 1.25rem">Masuk ke Akunmu</b>
        </div>
        <x-inputs.input 
            label="Email" 
            name="email" 
            type="email"
            placeholder="Masukkan Email" 
            required
        />
        <x-inputs.input 
            label="Password" 
            name="password" 
            placeholder="Masukkan password" 
            type="password"
            required
        />
        <button type="submit" class="btn btn-primary btn-block">Masuk</button>
        <a href="{{route('google')}}" class="btn btn-danger btn-block">Masuk dengan Google</a>
        <a href="{{route('register')}}" class="btn btn-outline-primary btn-block">Daftar</a>
    </div>
</form>