<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="{{ route('home') }}">
        <b>Lingkar Belajar</b>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    @guest
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav ml-auto">
            <a class="nav-item nav-link {{Route::currentRouteName() == 'login' ? 'active' : ''}}" href="{{route('login')}}">Login</a>
            <a class="nav-item nav-link {{Route::currentRouteName() == 'register' ? 'active' : ''}}" href="{{route('register')}}">Register</a>
        </div>
    </div>    
    @else
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav ml-auto">
            <a class="nav-item nav-link {{Route::currentRouteName() == 'forums.index' ? 'active' : ''}}" href="{{route('forums.index')}}"><i class="fas fa-users"></i> Forum</a>
            <a class="nav-item nav-link {{Route::currentRouteName() == 'documents.index' ? 'active' : ''}}" href="{{route('documents.index')}}"><i class="fas fa-file-alt"></i> Dokumen</a>
        </div>
        <div class="navbar-nav ml-auto">
            <a class="nav-item nav-link {{Route::currentRouteName() == 'profiles.show' ? (basename(Request::url()) == Auth::id() ? 'active' : '' ) : ''}}" href="{{ route('profiles.show', Auth::id()) }}">{{Auth::user()->name}}</a>
        </div>
    </div>
    @endguest
</nav>