<div {{ $attributes->merge() }}>
    <div class="card mb-4">
        <div class="card-body">
            <small>Pangkat saya</small>
            <h5 class="font-weight-bold">{{Auth::user()->rank}}</h5>
            <div class="w-100 text-truncate">
                {{Auth::user()->point}} poin &#8231; 
                {{Auth::user()->forums->count()}} diskusi &#8231; 
                {{Auth::user()->comments->count()}} kontribusi
            </div>
        </div>
    </div>
    <div class="px-3">
        <h5 class="font-weight-bold">Klasemen Mahasiswa</h5>
        <hr>
        @foreach ($students as $student)
            <div class="d-flex align-items-center flex-fill mb-3" style="min-width: 0">
                <span class="font-weight-bold" style="width: 25px">
                    {{$loop->iteration}}
                </span>
                <img style="height: 35px; width:35px; object-fit:cover" src="{{ Avatar::create($student->name)->toBase64() }}" alt="..." class="rounded-circle mr-3">
                <div class="d-flex flex-column flex-fill" style="min-width: 0">
                    <a href="{{ route('profiles.show', $student->id) }}" class="font-weight-bold mb-0 text-truncate text-dark">
                        {{$student->name}}
                    </a>
                    <small class="text-truncate">
                        {{$student->point}} poin
                    </small>
                </div>
            </div>
        @endforeach
    </div>
</div>