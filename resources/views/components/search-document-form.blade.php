<div {{ $attributes->merge(['class' => 'card']) }}>
    <form class="card-body" action="{{route('documents.search')}}">
        <x-inputs.input 
            label="Cari dokumen" 
            name="keyword" 
            placeholder="Masukkan kata kunci"
            :value="request()->keyword"
        />
        <x-inputs.select 
            label="Mata Kuliah" 
            name="subjectIds" 
            placeholder="Semua mata kuliah"
            search
            :value="request()->subjectIds"
            :option-values=" $subjects->map(fn($subject) => $subject->id)"
            :option-names=" $subjects->map(fn($subject) => $subject->name)" 
            multiple
        />
        <x-inputs.select 
            label="Tipe Dokumen" 
            name="typeIds" 
            placeholder="Semua tipe"
            search
            :value="request()->typeIds"
            :option-values=" $types->map(fn($type) => $type->id)"
            :option-names=" $types->map(fn($type) => $type->name)" 
            multiple
        />
        <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-search"></i> Cari</button>
    </form>
</div>