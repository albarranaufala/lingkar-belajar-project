<div {{ $attributes->merge(['class' => 'card']) }}>
    <form class="card-body" action="{{route('forums.search')}}">
        <x-inputs.input 
            label="Cari diskusi" 
            name="keyword" 
            placeholder="Masukkan kata kunci"
            :value="request()->keyword"
        />
        <x-inputs.select 
            label="Mata Kuliah" 
            name="subjectIds" 
            placeholder="Semua mata kuliah"
            search
            :value="request()->subjectIds"
            :option-values=" $subjects->map(fn($subject) => $subject->id)"
            :option-names=" $subjects->map(fn($subject) => $subject->name)" 
            multiple
        />
        <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-search"></i> Cari</button>
    </form>
</div>