@extends('layouts.app', [
    'title' => 'Dokumen'
])

@section('content')
<div class="container-fluid pt-3">
    <div class="row justify-content-center mb-3">
        <div class="col-md-6">
            <x-add-document 
                :subjects="$subjects"
                :types="$types"
            />
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-3 mb-3">
            <x-search-document-form
                :subjects="$subjects"
                :types="$types"
            />
        </div>
        <div class="col-md-6">
            @forelse($documents->sortByDesc('created_at') as $document)
                <x-document-item class="mb-3"
                    :document="$document"   
                />
            @empty
            <div class="w-100 text-center">
                Tidak ada dokumen
            </div>
            @endforelse
        </div>
        <div class="col-md-3 mb-3 d-md-block d-none">
            <x-scoreboard
                :students="$firstFiveStudents"
            />
        </div>
    </div>
</div>
@endsection
