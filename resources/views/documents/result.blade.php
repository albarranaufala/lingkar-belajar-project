@extends('layouts.app')

@section('content')
<div class="container-fluid pt-3">
    <div class="row justify-content-center mb-3">
        <div class="col-md-6">
            <h3 class="mb-0 font-weight-bold">
                {{$documentResults->count()}} Dokumen Ditemukan
            </h3>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-3 mb-3">
            <x-search-document-form
                :subjects="$subjects"
                :types="$types"
            />
        </div>
        <div class="col-md-6">
            @forelse ($documentResults->sortByDesc('created_at') as $document)
                <x-document-item class="mb-3"
                    :document="$document"   
                />
            @empty
                Tidak ada dokumen
            @endforelse
        </div>
        <div class="col-md-3">
            {{-- Scoreboard --}}
        </div>
    </div>
</div>
@endsection