@extends('layouts.app', [
    'title' => $document->title
])

@section('content')
<div class="container pt-3">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mb-3">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-12 d-flex justify-content-between">
                            <div>
                                <a class="badge badge-secondary px-2" href="{{route('documents.search', ['subjectIds' => $document->subject->id])}}">
                                    {{$document->subject->name}}
                                </a>
                                <a class="badge badge-success px-2" 
                                    href="{{route('documents.search', ['typeIds' => $document->type->id])}}">
                                    {{$document->type->name}}
                                </a>
                            </div>
                            <small class="text-truncate">{{$document->created_at->diffForHumans()}}</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <h4 class="font-weight-bold">
                                {{$document->title}}
                            </h4>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12">
                            <p class="mb-0">{{$document->description}}</p>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12">
                            @foreach ($document->fileArray() as $item)
                                <div class="card bg-info text-light mb-2">
                                    <div class="card-body d-flex justify-content-between">
                                        <div>
                                            {{basename($item)}}
                                        </div>
                                        <div>
                                            <a href="{{url(Storage::url($item))}}" target="_blank">
                                                <i class="fas fa-eye text-light"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 d-flex justify-content-between align-items-end">
                            <div class="d-flex align-items-center flex-fill" style="min-width: 0">
                                <img style="height: 40px; width:40px; object-fit:cover" src="{{ Avatar::create($document->user->name)->toBase64() }}" alt="..." class="rounded-circle mr-3">
                                <div class="d-flex flex-column flex-fill" style="min-width: 0">
                                    <a href="{{ route('profiles.show', ['user' => $document->user->id]) }}" class="font-weight-bold mb-0 text-truncate">
                                        {{ $document->user->name }}
                                    </a>
                                    <small class="text-truncate">
                                        {{$document->user->majority->name}} 
                                        &#8231; 
                                        {{$document->user->university->name}}
                                    </small>
                                </div>
                            </div>
                            <div class="d-flex align-items-center">
                                <i id="btn-recommend" 
                                    class="{{Auth::user()->recommendDocument($document)->count() ?
                                                (Auth::user()->recommendDocument($document)->first()->status == App\PostRecommend::STATUS_RECOMMENDED ?
                                                    'fas text-danger'
                                                    : 'far')
                                                : 'far'
                                            }} fa-heart mr-1" style="cursor: pointer"></i>
                                <div id="recommend-count" class="d-inline mr-3">
                                    {{$document->recommends->count()}}
                                </div>
                                <i class="far fa-comment-alt mr-1"></i>
                                <div class="comment-count d-inline">
                                    {{$document->comments->count()}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="my-4">
                    <div class="row">
                        <form class="col-12" method="POST" action="{{route('documents.comments.store', $document->id)}}">
                            @csrf
                            <p>Pendapat Saya</p>
                            <div class="d-flex align-items-center mb-3">
                                <img style="height: 40px; width:40px; object-fit:cover" src="{{ Avatar::create(Auth::user()->name)->toBase64() }}" alt="..." class="rounded-circle mr-3">
                                <div class="d-flex flex-column">
                                    <a href="{{ route('profiles.show', ['user' => Auth::user()->id]) }}" class="font-weight-bold mb-0">
                                        {{Auth::user()->name}}
                                    </a>
                                    <small>
                                        {{Auth::user()->majority->name}} 
                                        &#8231; 
                                        {{Auth::user()->university->name}}
                                    </small>
                                </div>
                            </div>
                            <x-inputs.textarea 
                                name="message" 
                                placeholder="Tulis pendapat saya..."
                                rows="2"
                            />
                            <div class="w-100 text-right">
                                <button type="submit" class="btn btn-primary mb-3">Kirim</button>
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p>Pendapat ({{$document->comments->count()}})</p>
                            @forelse ($document->comments as $comment)
                                <div class="mt-5">
                                    <div class="d-flex align-items-start justify-content-between">
                                        <div class="d-flex align-items-center mb-3">
                                            <img style="height: 40px; width:40px; object-fit:cover" src="{{ Avatar::create($comment->user->name)->toBase64() }}" alt="..." class="rounded-circle mr-3">
                                            <div class="d-flex flex-column">
                                                <a href="{{ route('profiles.show', ['user' => $comment->user->id]) }}" class="font-weight-bold mb-0">
                                                    {{$comment->user->name}}
                                                </a>
                                                <small>
                                                    {{$comment->user->majority->name}} 
                                                    &#8231; 
                                                    {{$comment->user->university->name}}
                                                </small>
                                            </div>
                                        </div>
                                        <small>{{$comment->created_at->diffForHumans()}}</small>
                                    </div>
                                    <p class="mb-0">{{$comment->message}}</p>    
                                </div>
                            @empty
                                Tidak ada pendapat.
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
         $(document).ready(function() {
            const btnRecommend = document.getElementById('btn-recommend');

            btnRecommend.addEventListener('click', function(e){
                fetch('{{route('documents.recommend', $document->id)}}', {
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "application/json, text-plain, */*",
                        "X-Requested-With": "XMLHttpRequest",
                        "X-CSRF-TOKEN": '{{csrf_token()}}'
                        },
                    method: 'post',
                    credentials: "same-origin",
                })
                .then(response => response.json())
                .then((response) => {
                    const recommendCount = response.data.recommendCount;
                    const userRecommend = response.data.userRecommend;
                    const recommendCountDOM = document.getElementById('recommend-count');

                    btnRecommend.classList.toggle('text-danger');
                    btnRecommend.classList.toggle('far');
                    btnRecommend.classList.toggle('fas');
                    recommendCountDOM.textContent = recommendCount;
                })
                .catch(function(error) {
                    console.log(error);
                })
            });
        });
    </script>
@endpush