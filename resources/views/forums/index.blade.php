@extends('layouts.app', [
    'title' => 'Home'
])

@section('content')
<div class="container-fluid pt-3">
    <div class="row justify-content-center mb-3">
        <div class="col-md-6">
            <x-add-discussion :subjects="$subjects"/>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-3 mb-3">
            <x-search-forum-form
                :subjects="$subjects"
            />
        </div>
        <div class="col-md-6">
            @forelse($forums->sortByDesc('created_at') as $forum)
                <x-discussion-item class="mb-3"
                    :forum="$forum"   
                />
            @empty
                <div class="w-100 text-center">
                    Tidak ada diskusi
                </div>
            @endforelse
        </div>
        <div class="col-md-3 mb-3 d-md-block d-none">
            <x-scoreboard
                :students="$firstFiveStudents"
            />
        </div>
    </div>
</div>
@endsection
