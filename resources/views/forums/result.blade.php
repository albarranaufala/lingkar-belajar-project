@extends('layouts.app', [
    'title' => 'Pencarian'
])

@section('content')
<div class="container-fluid pt-3">
    <div class="row justify-content-center mb-3">
        <div class="col-md-6">
            <h3 class="mb-0 font-weight-bold">
                {{$forumResults->count()}} Forum Ditemukan
            </h3>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-3">
            <x-search-forum-form
                :subjects="$subjects"
            />
        </div>
        <div class="col-md-6">
            @forelse ($forumResults->sortByDesc('created_at') as $forum)
                <x-discussion-item class="mb-3"
                    :forum="$forum"   
                />
            @empty
                Tidak ada diskusi
            @endforelse
        </div>
        <div class="col-md-3">
            {{-- Scoreboard --}}
        </div>
    </div>
</div>
@endsection