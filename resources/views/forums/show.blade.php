@extends('layouts.app', [
    'title' =>  $forum->title 
])

@section('content')
<div class="container pt-3">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mb-3">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-12 d-flex justify-content-between">
                            <div>
                                @foreach($forum->subjects()->data as $subject)
                                    <a class="badge badge-secondary px-2" href="{{route('forums.search', ['subjectIds' => $subject->id])}}">
                                        {{$subject->name}}
                                    </a>
                                @endforeach
                            </div>
                            <small class="text-truncate">{{$forum->created_at->diffForHumans()}}</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <h4 class="font-weight-bold">
                                {{$forum->title}}
                            </h4>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12">
                            <p>{{$forum->description}}</p>
                        </div>
                    </div>
                    @if ($forum->attachment)
                    <div class="row mb-5">
                        <div class="col-12">
                            <img src="{{url(Storage::url($forum->attachment))}}" alt="" class="w-100 rounded">
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-12 d-flex justify-content-between align-items-end">
                            <div class="d-flex align-items-center flex-fill" style="min-width: 0">
                                <img style="height: 40px; width:40px; object-fit:cover" src="{{ Avatar::create($forum->user->name)->toBase64() }}" alt="..." class="rounded-circle mr-3">
                                <div class="d-flex flex-column flex-fill" style="min-width: 0">
                                    <a href="{{ route('profiles.show', ['user' => $forum->user->id ]) }}" class="font-weight-bold mb-0 text-truncate">
                                        {{ $forum->user->name }}
                                    </a>
                                    <small class="text-truncate">
                                        {{$forum->user->majority->name}} 
                                        &#8231; 
                                        {{$forum->user->university->name}}
                                    </small>
                                </div>
                            </div>
                            <div class="d-flex align-items-center">
                                <i id="btn-recommend" 
                                    class="{{Auth::user()->recommendForum($forum)->count() ?
                                                (Auth::user()->recommendForum($forum)->first()->status == App\PostRecommend::STATUS_RECOMMENDED ?
                                                    'fas text-danger'
                                                    : 'far')
                                                : 'far'
                                            }} fa-heart mr-1" style="cursor: pointer"></i>
                                <div id="recommend-count" class="d-inline mr-3">
                                    {{$forum->recommends->count()}}
                                </div>
                                <i class="far fa-comment-alt mr-1"></i>
                                <div class="comment-count d-inline">
                                    {{$forum->comments->count()}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="my-4">
                    <div class="row">
                        <form class="col-12" method="POST" action="{{route('forums.comments.store', $forum->id)}}">
                            @csrf
                            <p>Pendapat Saya</p>
                            <div class="d-flex align-items-center mb-3">
                                <img style="height: 40px; width:40px; object-fit:cover" src="{{ Avatar::create(Auth::user()->name)->toBase64() }}" alt="..." class="rounded-circle mr-3">
                                <div class="d-flex flex-column">
                                    <a href="{{ route('profiles.show', ['user' => Auth::user()->id]) }}" class="font-weight-bold mb-0">
                                        {{Auth::user()->name}}
                                    </a>
                                    <small>
                                        {{Auth::user()->majority->name}} 
                                        &#8231; 
                                        {{Auth::user()->university->name}}
                                    </small>
                                </div>
                            </div>
                            <x-inputs.textarea 
                                name="message" 
                                placeholder="Tulis pendapat saya..."
                                rows="2"
                            />
                            <div class="w-100 text-right">
                                <button type="submit" class="btn btn-primary mb-3">Kirim</button>
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p>Pendapat ({{$forum->comments->count()}})</p>
                            @forelse ($forum->comments as $comment)
                                <div class="mt-5">
                                    <div class="d-flex align-items-start justify-content-between">
                                        <div class="d-flex align-items-center mb-3">
                                            <img style="height: 40px; width:40px; object-fit:cover" src="{{ Avatar::create($comment->user->name)->toBase64() }}" alt="..." class="rounded-circle mr-3">
                                            <div class="d-flex flex-column">
                                                <a href="{{ route('profiles.show', ['user' => $comment->user->id]) }}" class="font-weight-bold mb-0">
                                                    {{$comment->user->name}}
                                                </a>
                                                <small>
                                                    {{$comment->user->majority->name}} 
                                                    &#8231; 
                                                    {{$comment->user->university->name}}
                                                </small>
                                            </div>
                                        </div>
                                        <small>{{$comment->created_at->diffForHumans()}}</small>
                                    </div>
                                    <p class="mb-0">{{$comment->message}}</p>    
                                    <div class="float-right">
                                        <i id="btn-recommend-comment" 
                                            data-id="{{ $comment->id }}"
                                            class="btn-recommend-comment {{Auth::user()->recommendCommentForum($comment)->count() ?
                                                        (Auth::user()->recommendCommentForum($comment)->first()->status == App\CommentRecommend::STATUS_RECOMMENDED ?
                                                            'fas text-danger'
                                                            : 'far')
                                                        : 'far'
                                                    }} fa-heart mr-1" style="cursor: pointer"></i>
                                        <div id="recommend-comment-count" class="d-inline recommend-comment-count">
                                            {{$comment->recommends->count()}}
                                        </div>
                                    </div>
                                </div>
                            @empty
                                Tidak ada pendapat.
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
         $(document).ready(function() {
            // Recommend POST
            const btnRecommend = document.getElementById('btn-recommend');

            btnRecommend.addEventListener('click', function(e){
                fetch('{{route('forums.recommend', $forum->id)}}', {
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "application/json, text-plain, */*",
                        "X-Requested-With": "XMLHttpRequest",
                        "X-CSRF-TOKEN": '{{csrf_token()}}'
                        },
                    method: 'post',
                    credentials: "same-origin",
                })
                .then(response => response.json())
                .then((response) => {
                    const recommendCount = response.data.recommendCount;
                    const userRecommend = response.data.userRecommend;
                    const recommendCountDOM = document.getElementById('recommend-count');

                    btnRecommend.classList.toggle('text-danger');
                    btnRecommend.classList.toggle('far');
                    btnRecommend.classList.toggle('fas');
                    recommendCountDOM.textContent = recommendCount;
                })
                .catch(function(error) {
                    console.log(error);
                })
            });

            // Recommend COMMENT    
            btnRecommendComments = document.querySelectorAll('.btn-recommend-comment');
            btnRecommendComments.forEach(btn => {
                btn.addEventListener('click', function(e){
                    let comment_id = btn.dataset.id;
                    
                    fetch('/forums/{{$forum->id}}/comments/'+comment_id+'/recommend', {
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json, text-plain, */*",
                            "X-Requested-With": "XMLHttpRequest",
                            "X-CSRF-TOKEN": '{{csrf_token()}}'
                            },
                        method: 'post',
                        credentials: "same-origin",
                    })
                    .then(response => response.json())
                    .then((response) => {
                        const userRecommendComment = response.data.userRecommend;
                        const recommendCommentCount = response.data.recommendCount;
    
                        btn.classList.toggle('text-danger');
                        btn.classList.toggle('far');
                        btn.classList.toggle('fas');
                        btn.parentElement.querySelector('.recommend-comment-count').textContent = recommendCommentCount;
                    })
                    .catch(function(error) {
                        console.log(error);
                    })
                });
            });
        });
    </script>
@endpush