<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- Icon --}}
    <link rel="icon" href="{{asset('img/logo-sm.png')}}" type="image/gif" sizes="16x16">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ (isset($title) ? $title.' - ' : ''). config('app.name') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('style')
    <style>
        .footer {
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 50px;
        }    
    </style>
</head>
<body>
    <div id="app" class="min-vh-100 position-relative" style="padding-bottom:50px">
        <header class="sticky-top">
            <x-navbar/>
        </header>

        <main>
            <ul class="position-fixed list-unstyled notification d-none" style="z-index: 999; right:0">
                @foreach (session('notifications', []) as $notification)
                    <li class="m-3">
                        <x-alert
                            :type="$notification->type"
                            :message="$notification->message"
                        />
                    </li>
                @endforeach
                @foreach ($errors->all() as $error)
                    <li class="m-3">
                        <x-alert
                            type="danger"
                            :message="$error"
                        />
                    </li>
                @endforeach
            </ul>

            @yield('content')
        </main>
        <footer class="footer bg-primary text-light d-flex align-items-center justify-content-center py-3">
            <small>Developed with ❤️ by Lingkar Belajar</small>
        </footer>
    </div>
    
    @stack('scripts')

    <script>
        $(document).ready(function() {
            $(".notification").fadeIn("slow").removeClass("d-none");
        });
    </script>
</body>
</html>
