@extends('layouts.app', [
    'title' => 'Pengaturan Akun'
])

@section('content')
    <div class="container mt-3">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-body">
                    <div class="text-center my-3">
                        <b style="font-size: 1.25rem">Ubah Data Akun</b>
                    </div>
                    <form method="POST" action="{{route('profiles.update')}}" class="card-body" >
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <x-inputs.input 
                                    label="Nama" 
                                    name="name" 
                                    type="text"
                                    :value="Auth::user()->name"
                                    placeholder="Masukkan Nama Anda"
                                    required
                                />
                                <x-inputs.input 
                                    label="Email" 
                                    name="email" 
                                    type="email"
                                    :value="Auth::user()->email"
                                    placeholder="Masukkan Email Anda"
                                    required
                                />
                            </div>
                            <div class="col-md-6">
                                <x-inputs.select 
                                    required 
                                    label="Jurusan" 
                                    name="majority" 
                                    placeholder="Pilih jurusan"
                                    :value="Auth::user()->majority->id"
                                    search
                                    :option-values=" $majorities->map(fn($majority) => $majority->id)"
                                    :option-names=" $majorities->map(fn($majority) => $majority->name)" 
                                />
                                <x-inputs.select 
                                    required 
                                    label="Universitas" 
                                    name="university" 
                                    placeholder="Pilih universitas"
                                    :value="Auth::user()->university->id"
                                    search
                                    :option-values=" $universities->map(fn($university) => $university->id)"
                                    :option-names=" $universities->map(fn($university) => $university->name)" 
                                />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-right">
                                <a href="" class="btn btn-secondary mr-2">Ganti Password</a>
                                <a href="{{route('profiles.show', Auth::id())}}" class="btn btn-light mr-2">Batal</a>
                                <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection