@extends('layouts.app', [
    'title' => $user->name
])

@section('content')
    <div class="container mt-3">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="my-3 d-flex align-items-center flex-md-row flex-column">
                    <div class="m-3">
                        <img style="height: 120px; width:120px; object-fit:cover" src="{{ Avatar::create($user->name)->toBase64() }}" alt="..." class="rounded-circle">
                    </div>
                    <div>
                        <div class="mb-2 d-flex flex-wrap align-items-center justify-content-center justify-content-md-start">
                            <h3 class="font-weight-bold mb-2 mx-2 text-center text-md-left">
                                {{ $user->name }}
                            </h3>
                            @if ($user->id == Auth::id())
                            <div class="d-flex mb-2">
                                <a href="{{route('profiles.edit')}}" class="mx-2 btn btn-outline-primary btn-sm">
                                    <i class="fas fa-edit"></i>
                                    Edit Profil
                                </a>
                                <form action="{{ route('logout') }}" method="POST">
                                    @csrf
                                    <button class="btn btn-outline-danger btn-sm mr-2">Logout</button>
                                </form>
                            </div>
                            @endif
                        </div>
                        <div class="d-flex justify-content-center justify-content-md-start text-center text-md-left mx-2">
                            {{ $user->majority->name }} 
                            &#8231; 
                            {{ $user->university->name }}
                        </div>
                    </div>
                </div>
                <div class="card my-3">
                    <div class="card-body">
                        <small class="text-truncate">Pangkat saya</small>
                        <h4 class="font-weight-bold mb-2 mt-1">
                            {{ $user->rank }}
                        </h4>
                        <div class="text-truncate">
                            {{ $user->point }} Poin &#8231; 
                            {{ $user->forums->count() }} Diskusi &#8231; 
                            {{ $user->comments->count() }} Kontribusi
                        </div>
                    </div>
                </div>
                <div>
                    @forelse ($user->activities as $activity)
                        @if ($activity->activityType == 'forum')
                            <x-discussion-item class="mb-3 border-primary"
                                :forum="$activity"
                            ></x-discussion-item>
                        @else
                            <x-document-item class="mb-3 border-success"
                                :document="$activity"
                            ></x-document-item>
                        @endif
                    @empty
                        Belum ada aktivitas
                    @endforelse
                </div>
            </div>
        </div>
    </div>
@endsection