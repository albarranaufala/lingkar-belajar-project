<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', function () {
    if(Auth::check()){
        return redirect(route('forums.index'));
    } else {
        return redirect(route('login'));
    }
})->name('home');

// Search
Route::get('forums/search', 'SearchController@searchForum')->name('forums.search');
Route::get('documents/search', 'SearchController@searchDocument')->name('documents.search');

// Documents 
Route::get('/documents', 'DocumentController@index')->name('documents.index');
Route::resource('documents', 'DocumentController')->except('create');
Route::prefix('documents/{document}')->name('documents.')->group(function () {
    Route::post('comments', 'DocumentCommentController@store')->name('comments.store');
    Route::post('recommend', 'RecommendController@recommendDocument')->name('recommend');
});
// Forums 
Route::resource('forums', 'ForumController')->except('create');

Route::prefix('forums/{forum}')->name('forums.')->group(function () {
    Route::post('comments', 'ForumCommentController@store')->name('comments.store');
    Route::post('recommend', 'RecommendController@recommendForum')->name('recommend');
    Route::post('comments/{comment}/recommend', 'RecommendController@recommendCommentForum')->name('comments.recommend');
});

// Google Auth 
Route::get('google', 'GoogleController@redirect')->name('google');
Route::get('callback/google', 'GoogleController@callback');

// Profil
Route::prefix('profiles')->name('profiles.')->group(function () {
    Route::get('/edit', 'ProfileController@edit')->name('edit');
    Route::post('/edit', 'ProfileController@update')->name('update');
    Route::get('{user}', 'ProfileController@show')->name('show');
});

Route::get('register/complete', 'Auth\CompleteRegisterController@completeRegister')
    ->name('register.complete');
Route::post('register/complete', 'Auth\CompleteRegisterController@storeCompleteRegister')
    ->name('register.complete.store');